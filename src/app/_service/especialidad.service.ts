import { Especialidad } from './../_model/especialidad';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HOST } from './../_shared/var.constant';

@Injectable()
export class EspecialidadService {
    url: string = `${HOST}/especialidad`;

    constructor(private http: HttpClient) {

    }

    public getListarEspecialidad() {
        return this.http.get<Especialidad[]>(`${this.url}/listar`);
    }

}